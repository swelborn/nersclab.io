nav:
  - Home: index.md
  - Getting Started: getting-started.md
  - Tutorials:
      - tutorials/index.md
      - Playbooks:
        - tutorials/playbooks/index.md
        - Compiling a code: tutorials/playbooks/compiling.md
  - Accounts:
      - accounts/index.md
      - Passwords: accounts/passwords.md
      - Policy: accounts/policy.md
      - Collaboration Accounts: accounts/collaboration_accounts.md
  - Iris:
      - iris/index.md
      - Users: iris/iris-for-users.md
      - PIs and Project Managers: iris/iris-for-pis.md
      - ERCAP and Iris Guide for Allocation Managers: iris/iris-for-allocation-managers.md
      - Publications Tracking in Iris: iris/iris-publications.md
  - Systems:
      - systems/index.md
      - Perlmutter:
        - Using Perlmutter: systems/perlmutter/index.md
        - Architecture: systems/perlmutter/architecture.md
        - Software: systems/perlmutter/software/index.md
        - Running Jobs: systems/perlmutter/running-jobs/index.md
        - Finding and using software: systems/perlmutter/software/finding-software.md
        - Timeline: systems/perlmutter/timeline/index.md
      - Data Transfer Nodes:
        - systems/dtn/index.md
  - Storage Systems:
      - filesystems/index.md
      - Unix File Permissions: filesystems/unix-file-permissions.md
      - PI Toolbox: filesystems/pitoolbox.md
      - Quotas: filesystems/quotas.md
      - Perlmutter scratch: filesystems/perlmutter-scratch.md
      - Community: filesystems/community.md
      - Archive (HPSS):
        - Introduction: filesystems/archive.md
        - Storage Statistics: filesystems/archive-stats.md
      - Global Home: filesystems/global-home.md
      - Global Common: filesystems/global-common.md
      - Backups: filesystems/backups.md
      - DnA: filesystems/dna.md
  - Connecting:
      - connect/index.md
      - Multi-Factor Authentication: connect/mfa.md
      - Federated Identity: connect/federatedid.md
      - NoMachine / NX, X Windows Accelerator: connect/nx.md
      - Visual Studio Code / VSCode: connect/vscode.md
  - Environment:
      - environment/index.md
      - Shell Startup: environment/shell_startup.md
      - Lmod: environment/lmod.md
  - Policies:
    - policies/index.md
    - Resource Usage Policies: policies/resource-usage.md
    - Data Policy: policies/data-policy/policy.md
    - Software Support Policy:
      - Software Support Policy:
        - policies/software-policy/index.md
      - Frequently Asked Questions: policies/software-policy/faq.md
    - NERSC Center Policies: "https://www.nersc.gov/users/policies/"
  - Development:
    - Compilers:
        - development/compilers/index.md
        - Base Compilers: development/compilers/base.md
        - Compiler Wrappers (recommended): development/compilers/wrappers.md
    - Build Tools:
        - Autoconf and Make: development/build-tools/autoconf-make.md
        - CMake: development/build-tools/cmake.md
        - Spack: development/build-tools/spack.md
    - Programming Models:
        - development/programming-models/index.md
        - MPI:
          - development/programming-models/mpi/index.md
          - Cray MPICH: development/programming-models/mpi/cray-mpich.md
          - Open MPI: development/programming-models/mpi/openmpi.md
          - Upstream MPICH: development/programming-models/mpi/mpich.md
        - OpenMP:
          - development/programming-models/openmp/index.md
          - Tools for OpenMP: development/programming-models/openmp/openmp-tools.md
        - OpenACC:
          - development/programming-models/openacc/index.md
        - CUDA:
          - development/programming-models/cuda/index.md
        - UPC: development/programming-models/upc.md
        - UPC++: development/programming-models/upcxx.md
        - Coarrays: development/programming-models/coarrays.md
        - SYCL:
          - development/programming-models/sycl/index.md
        - Kokkos: development/programming-models/kokkos.md
        - HPX: development/programming-models/hpx.md
        - C++ parallel algorithms benchmark: development/programming-models/iso-cpp-parstl-benchmark/cppParSTL.md
    - Languages:
        - Julia: development/languages/julia.md
        - R: development/languages/r.md
        - IDL: development/languages/idl.md
        - CERN ROOT: development/languages/cernroot.md
        - Python:
            - development/languages/python/index.md
            - Intro to Python at NERSC: development/languages/python/python-intro.md
            - Using Python at NERSC: development/languages/python/nersc-python.md
            - Parallel Python: development/languages/python/parallel-python.md
            - Python in Shifter: development/languages/python/python-shifter.md
            - Profiling Python: development/languages/python/profiling-debugging-python.md
            - Python on AMD CPUs: development/languages/python/python-amd.md
            - Using Python on Perlmutter: development/languages/python/using-python-perlmutter.md
            - Porting Python to Perlmutter GPUs: development/languages/python/perlmutter-prep.md
            - FAQ and Troubleshooting: development/languages/python/faq-troubleshooting.md
    - Libraries:
        - development/libraries/index.md
        - FFTW:
          - development/libraries/fftw/index.md
        - LAPACK:
          - development/libraries/lapack/index.md
        - MKL:
          - development/libraries/mkl/index.md
        - LibSci:
          - development/libraries/libsci/index.md
        - HDF5:
          - development/libraries/hdf5/index.md
        - NetCDF:
          - development/libraries/netcdf/index.md
    - Containers:
        - development/containers/index.md
        - Podman-hpc:
            - Podman-hpc for Beginners Tutorial: development/containers/podman-hpc/podman-beginner-tutorial.md
            - Using Podman-hpc at NERSC: development/containers/podman-hpc/overview.md
        - Shifter:
            - development/containers/shifter/index.md
            - Shifter for Beginners Tutorial: development/containers/shifter/shifter-beginner-tutorial.md
            - How to use Shifter: development/containers/shifter/how-to-use.md
            - FAQ and Troubleshooting: development/containers/shifter/faq-troubleshooting.md
            - Example Shifter Containers: development/containers/shifter/examples.md
        - registry.nersc.gov:
            - development/containers/registry/index.md
  - Developer Tools:
      - tools/index.md
      - Performance Tools:
          - tools/performance/index.md
          - Codee:
            - tools/performance/codee/index.md
          - CrayPat:
            - tools/performance/craypat/index.md
          - Darshan:
            - tools/performance/darshan/index.md
            - DXT Explorer: tools/performance/darshan/dxt.md
            - Drishti: tools/performance/darshan/drishti.md
          - MAP:
            - tools/performance/map/index.md
          - Performance Reports:
            - tools/performance/performancereports/index.md
          - Reveal:
            - tools/performance/reveal/index.md
          - Roofline Performance Model:
            - tools/performance/roofline/index.md
      - Debug Tools:
          - tools/debug/index.md
          - DDT:
            - tools/debug/ddt/index.md
          - GDB:
            - tools/debug/gdb/index.md
          - gdb4hpc and CCDB:
            - tools/debug/gdb4hpc_ccdb/index.md
          - STAT and ATP:
            - tools/debug/stat_atp/index.md
          - TotalView:
            - tools/debug/totalview/index.md
          - Valgrind:
            - tools/debug/valgrind/index.md
  - Running Jobs:
    - jobs/index.md
    - Scheduling: jobs/scheduling.md
    - Queues and Charges: jobs/policy.md
    - Example Jobs: jobs/examples/index.md
    - Jobscript Generator: jobs/jobscript-generator.md
    - Best Practices: jobs/best-practices.md
    - Troubleshooting Jobs: jobs/troubleshooting.md
    - Monitoring: jobs/monitoring.md
    - Affinity:
      - jobs/affinity/index.md
    - Interactive: jobs/interactive.md
    - Reservations: jobs/reservations.md
    - Workflow Tools:
      - jobs/workflow/index.md
      - Workflow queue: jobs/workflow/workflow-queue.md
      - Scrontab: jobs/workflow/scrontab.md
      - GNU Parallel: jobs/workflow/gnuparallel.md
      - TaskFarmer: jobs/workflow/taskfarmer.md
      - FireWorks: jobs/workflow/fireworks.md
      - Nextflow: jobs/workflow/nextflow.md
      - Papermill: jobs/workflow/papermill.md
      - Parsl: jobs/workflow/parsl.md
      - Snakemake: jobs/workflow/snakemake.md
      - Community Supported Tools: jobs/workflow/community_tools.md
    - Checkpoint/Restart:
      - Checkpoint/Restart:
        - development/checkpoint-restart/index.md
      - DMTCP:
        - development/checkpoint-restart/dmtcp/index.md
      - MANA:
        - development/checkpoint-restart/mana/index.md
  - Applications:
    - applications/index.md
    - AMBER:
      - applications/amber/index.md
    - Abinit:
      - applications/abinit/index.md
    - BerkeleyGW:
      - applications/berkeleygw/index.md
    - CP2K:
      - applications/cp2k/index.md
    - E4S:
      - applications/e4s/index.md
      - 23.08: applications/e4s/perlmutter/23.08.md
      - 23.05: applications/e4s/perlmutter/23.05.md
      - 22.11: applications/e4s/perlmutter/22.11.md
      - Spack-Gitlab-Pipeline: applications/e4s/spack_gitlab_pipeline.md
      - Spack Environments: applications/e4s/spack_environments.md
    - GAMESS:
      - applications/gamess/index.md
    - Gromacs:
      - applications/gromacs/index.md
    - LAMMPS:
      - applications/lammps/index.md
    - Mathematica:
      - applications/mathematica/index.md
    - MATLAB:
      - MATLAB:
        - applications/matlab/index.md
      - MATLAB Compiler: applications/matlab/matlab_compiler.md
    - MOLPRO:
      - applications/molpro/index.md
    - NAMD:
      - applications/namd/index.md
    - NCL:
      - applications/ncl/index.md
    - NWChem:
      - applications/nwchem/index.md
    - ORCA:
      - applications/orca/index.md
    - SIESTA:
      - applications/siesta/index.md
    - ParaView:
      - applications/paraview/index.md
    - Q-Chem:
      - applications/qchem/index.md
    - Quantum ESPRESSO:
      - applications/quantum-espresso/index.md
    - VASP:
      - applications/vasp/index.md
    - Wannier90:
      - applications/wannier90/index.md
    - WRF:
      - Introduction: applications/wrf/index.md
      - WPS: applications/wrf/wps.md
      - WRF: applications/wrf/wrf.md
      - WRF module: applications/wrf/wrf_module.md
      - WRF benchmark: applications/wrf/wrf_benchmark.md
      - Publications: applications/wrf/publications.md
#      - Shared Data: applications/wrf/shared_data.md
  - Analytics:
      - Analytics: analytics/analytics.md
      - Dask: analytics/dask.md
      - RStudio: services/rstudio.md
      - Spark: analytics/spark.md
  - Machine Learning:
      - machinelearning/index.md
      - TensorFlow: machinelearning/tensorflow.md
      - PyTorch: machinelearning/pytorch.md
      - TensorBoard: machinelearning/tensorboard.md
      - Distributed training:
        - machinelearning/distributed-training/index.md
      - Hyperparameter optimization: machinelearning/hpo.md
      - Science use-cases:
        - machinelearning/science-use-cases/index.md
        - HEP CNN: machinelearning/science-use-cases/hep-cnn.md
      - Current known issues: machinelearning/known_issues.md
  - Performance:
    - Perlmutter Readiness: performance/readiness.md
    - Vectorization: performance/vectorization.md
    - Parallelism: performance/parallelism.md
    - Compiler Diagnostics: performance/compiler-diagnostics.md
    - I/O:
        - DVS: performance/io/dvs.md
        - performance/io/index.md
        - I/O libraries: performance/io/library/index.md
        - Lustre: performance/io/lustre/index.md
        - File system in memory: performance/io/dev-shm.md
    - Portability: performance/portability.md
    - Variability: performance/variability.md
    - Network: performance/network.md
    - Case Studies:
        - performance/case-studies/index.md
        - GPU Case Studies:
            - performance/case-studies/gpu-case-studies/index.md
            - AMReX GPU Case Study: performance/case-studies/amrex-gpu/index.md
            - DESI GPU Case Study: performance/case-studies/desi/index.md
            - MetaHipMer GPU Case Study: performance/case-studies/metahipmer/index.md
            - Tomopy GPU Case Study: performance/case-studies/tomopy/index.md
            - SNAP GPU Case Study: performance/case-studies/snap/index.md
            - C++ ML Interface: performance/case-studies/CPP_2_Py/index.md
            - Lulesh with C++ Programming Models Case Study: performance/case-studies/lulesh-cpp/index.md
  - Services:
      - services/index.md
      - Spin:
        - services/spin/index.md
        - Terminology: services/spin/terminology.md
        - Getting Started:
          - Local Development: services/spin/local.md
          - Running Your App in Spin: services/spin/running.md
          - Attaching Storage: services/spin/storage.md
          - Connecting to a Spin App: services/spin/connecting.md
        - Advanced Concepts:
          - Using the Rancher CLI: services/spin/cli.md
          - Development and Production environments: services/spin/envs.md
          - Managing Spin Apps with Helm: services/spin/helm.md
          - Migrating Apps from Docker Compose: services/spin/migrate.md
        - FAQ: services/spin/faq.md
        - Examples: services/spin/examples.md

      - Science Gateways: services/science-gateways.md
      - API:
        - services/sfapi/index.md
        - Authentication: services/sfapi/authentication.md
        - Examples: services/sfapi/examples.md
        - Version: services/sfapi/versioning.md
      - Jupyter:
        - services/jupyter/index.md
        - How-To Guides: services/jupyter/how-to-guides.md
        - Reference: services/jupyter/reference.md
        - Background: services/jupyter/background.md
      - Gitlab: services/gitlab.md
      - Globus: services/globus.md
      - CVMFS: services/cvmfs.md
      - Frontier Cache: services/frontiercache.md
      - Databases: services/databases.md
      - GridFTP: services/gridftp.md
      - Scp: services/scp.md
      - Bbcp: services/bbcp.md
  - Acronyms: help/acronyms.md
  - Contributed Tips and Tricks: tips.md
  - Current Known Issues: current.md

# Project Information
site_name: NERSC Documentation
site_description: NERSC Documentation
site_author: NERSC
site_dir: public
site_url: "https://docs.nersc.gov/"
repo_name: GitLab/NERSC/docs
repo_url: https://gitlab.com/NERSC/nersc.gitlab.io
edit_uri: blob/main/docs/

# Configuration
strict: true

theme:
  name: material
  custom_dir: nersc-theme
  favicon: assets/images/favicon.ico
  logo: assets/images/logo.png
  language: en
  features:
    - navigation.indexes
    - content.action.edit

extra_javascript:
  - https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML
extra_css:
  - stylesheets/extra.css

extra:
  analytics:
    provider: google
    property: !ENV GA_TOKEN
  social:
    - icon: material/home-circle
      link: https://www.nersc.gov
    - icon: material/help-circle
      link: https://help.nersc.gov
    - icon: material/heart-pulse
      link: https://www.nersc.gov/live-status/motd/
    - icon: fontawesome/brands/github
      link: https://github.com/NERSC
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/NERSC
    - icon: fontawesome/brands/slack
      link: https://www.nersc.gov/users/NUG/nersc-users-slack/

# Extensions
markdown_extensions:
  - meta
  - attr_list
  - footnotes
  - admonition
  - codehilite:
      guess_lang: false
  - toc:
      permalink: true
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols:
      fractions: false
  - pymdownx.superfences
  - pymdownx.details
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - pymdownx.snippets

plugins:
  - search:
      separator: '[\s\-,:!=\[\]()"/]+|(?!\b)(?=[A-Z][a-z])|\.(?!\d)|&[lg]t;'
  - minify:
      minify_html: true
  - meta-descriptions
