# WRF on NERSC systems

## Introduction

The Weather Research and Forecasting (WRF) Model is a numerical model for the atmosphere and 
widely used in research projects, including those funded by the DOE Office of Science. 

WRF is very flexible in terms of scientific and computational configurations. The model 
can be configured to simulate a broad range of atmospheric processes from boundary-layer 
turbulence to global-scale circulations, and from a small domain on a single node to a large 
domain using 10s and 100s of nodes (1000s of cpus) of the NERSC system.

WRF Pre-Processing System (WPS) is a suite of programs to produce the initial and boundary 
conditions for WRF simulations. 

The model has been developed through a collaboration of several organizations and user community.
The source code is maintained in [Github](https://github.com/wrf-model) by the Mesoscale and 
Microscale Meteorology (MMM) Laboratory of the National Center for Atmospheric Research (NCAR). 

A WRF model user needs to download and compile the WRF and WPS source codes to use them 
on the NERSC sytems (i.e., the WRF model system is not provided as a pre-compiled module).

[Download WRF releases from here](https://github.com/wrf-model/WRF/releases)

[Download WPS releases from here](https://github.com/wrf-model/WPS/releases)

Documentation and general support for the WRF model are provided by MMM and the user community 
through the following websites:

[WRF v4.4 on-line user guide](https://www2.mmm.ucar.edu/wrf/users/docs/user_guide_v4/v4.4/contents.html)

[WRF and MPAS support forum](https://forum.mmm.ucar.edu/)

## WRF Special Interest Group (WRF-SIG) at NERSC

WRF-SIG is a group of WRF users at NERSC. We have quasi-regular meetings at every few months to 
exchange information and learn technical and scientific issues related to the WRF model and NERSC 
systems. The members will have access to the shared data directory and support tickets issued by 
other members to be aware of on-going technical issues for WRF at NERSC.

Please join the group through
[this google form](https://docs.google.com/forms/d/e/1FAIpQLSeu76SWSzUE9SSQhmbckB9WkBgpxD307YSmpFpv4_fsYvGzAg/viewform) 
and also subscribe to the
[Slack channel](https://nerscusers.slack.com/archives/C03E4P8NU7M) 
(it is required to first join
[the NERSC Users Slack workspace](https://www.nersc.gov/users/NUG/nersc-users-slack/)).

## Contents

!!! note
    The build and run instructions may change as the software environment at NERSC changes.

[Build WRF](./wrf.md#build-wrf)

[Run WRF](./wrf.md#run-wrf)

[Build WPS](./wps.md#build-wps)

[WRF and WPS modules](./wrf_module.md)

[WRF benchmark on NERSC systems](./wrf_benchmark.md)

[Publications by NERSC users using WRF](./publications.md)

[Shared WRF-related dataset created by NERSC users](./shared_data.md)
