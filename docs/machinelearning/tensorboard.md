# TensorBoard

[TensorBoard](https://pytorch.org/) provides tools for visualizing and
monitoring machine learning experiments. It is often used by TensorFlow, Keras
and PyTorch users.

You can run a TensorBoard client in your browser from https://jupyter.nersc.gov/.
In a separate notebook, choose any NERSC TensorFlow jupyter kernel, then run:

```python
import nersc_tensorboard_helper
%load_ext tensorboard
```

Run a tensorboard server from Jupyter by running the following command in a new
cell (note that port 0 asks TensorBoard to use a port not already in use):

```python
%tensorboard --logdir YOURLOGDIR --port 0
```

Calling the NERSC TB helper function will provide you with an address to
connect to the TensorBoard instance:

```python
nersc_tensorboard_helper.tb_address()
```
