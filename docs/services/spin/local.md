# Local Development

## Building Images

Before using Spin, if you plan to deploy custom software that
you have built, you should become familiar with how to write
Dockerfiles, build Docker container images for your application,
and push them to an image registry.

Typically, a build process will look something like this:

```bash
TAG="registry.nersc.gov/<myproject>/<myimage>:<mytag>"
docker build -t $TAG . && docker push $TAG
```

## Image Registries Supported by Spin

Spin can run application images from [Docker Hub](https://hub.docker.com/)
or from the NERSC image registry at https://registry.nersc.gov/.

Access to the NERSC image registry is typically set up during the SpinUp Workshop.

## Running Complex Apps Locally

If your application is composed of multiple containers, and you
want to continue developing locally in addition to deploying in
Spin, you may want to become familiar with a simple deployment tool
to use in your local development environment.  
There are several available tools you can use to do this.

### Docker Compose

Docker Compose, which
allows you to deploy _stacks_ of multiple containers using a
`docker-compose.yml` file on your workstation.

[Read more about working with Docker](https://docs.docker.com/get-started/).

### Rancher Desktop

Rancher Desktop provides the Docker runtime environment (much like Docker Desktop), 
as well as providing a local Kubernetes deployment.

[Read more about getting started with Rancher Desktop](https://rancherdesktop.io/).

### Podman Desktop

Podman Desktop is a graphical tool that lets you use the Podman container runtime environment
and work with local or remote Kubernetes clusters.

[Read more about getting started with Podman Desktop](https://podman-desktop.io/).
